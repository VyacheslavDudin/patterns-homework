import { SECONDS_TIMER_BEFORE_START_GAME, MAX_TEXT_ID } from "./config";
import { startGame } from "./gameProcess";
import { roomFacade } from "./rooms";

export const toggleReadyStatus = ({ io, socket, commentator }) => {
    let room = null;
    const user = socket.username;
    io.rooms.forEach((val, roomName) => {
        if (val.users.has(user)) {
            room = roomName;
        }
    });
    let readyStatus = io[room].readyStatus[user];
    readyStatus = !readyStatus;
    io[room].readyStatus[user] = readyStatus;
    io.in(room).emit("TOGGLE_INDICATORS", user, readyStatus);
    checkForTimerStart(io, room, commentator);
    
}

const getRandomIn = (start, end) => Math.round(Math.random() * (end - start) + start);

export const updateTimer = async (io, room, seconds, timerClass) => {
    const ONE_SEC = 1000;
    const timerFunc = () => new Promise((resolve) => {
        let interval = setInterval(() => {
            if (seconds < 0) {
                clearInterval(interval);
                resolve();
                return;
            }
            io[room].interval = interval;
            io.in(room).emit("UPDATE_TIMER", seconds--, timerClass);
        }, ONE_SEC);
    });
    await timerFunc();
}

export const checkForTimerStart = async (io, room, commentator) => {
    if (Object.values(io[room].readyStatus).every(ready => ready) && !(io[room].isTimerStarted || io[room].isGameStarted)) {
        io[room].isTimerStarted = true;
        io.rooms.get(room).visible = false;
        roomFacade.hideRoom(io, room);
        const id = getRandomIn(0, MAX_TEXT_ID);
        io.in(room).emit("TIMER_START", id);

        const roomMembers = (await getRoomClients(io, room)).map(id => ({ id, username: io.sockets.sockets[id].username }) );
        commentator.roomMembers.push(...roomMembers);
        commentator.greetings();
        await updateTimer(io, room, SECONDS_TIMER_BEFORE_START_GAME, 'timer');
        startGame(io, room, commentator);
    }
}


function getRoomClients(io, room) {
    return new Promise((resolve, reject) => {
      io.of('/').in(room).clients((error, clients) => {
        resolve(clients);
      });
    });
}


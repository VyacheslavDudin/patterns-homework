import { MAXIMUM_USERS_FOR_ONE_ROOM } from "./config";
import { checkForTimerStart } from "./gamePreparation";
import { finishGame } from "./gameProcess";

class RoomFacade {
  constructor() {

  }

  roomCreationHandler = (io, socket, title) => {
    const existingRoomTitles = Object.keys(Object.fromEntries(io.rooms.entries()));
    if (existingRoomTitles.indexOf(title) >= 0) {
      console.log('roomCreationError!');
      socket.emit("ROOM_CREATION_ERROR", title);
    }
    else {
      console.log(`UserName: ${socket.username} created and joined \"${title}\" room`);
      io[title] = { readyStatus: {}, isTimerStarted: false, isGameStarted: false, statistics: {}, rating: [] };
  
      const userList = new Set();
      userList.add(socket.username);
      io.rooms.set(title, { visible: true, users: userList });
      socket.join(title);
      io[title].readyStatus[socket.username] = false;
  
      io.sockets.emit("CREATE_ROOM", title);
      socket.emit("ENTER_ROOM", title, io[title].readyStatus, socket.username);
    }
  };

  hideRoom = (io, room) => {
    io.sockets.emit("HIDE_ROOM", room);
  }

  deleteRoom = (io, room) => {
    io.sockets.emit("DELETE_ROOM", room);
  }

  joinRoomHandler = (io, socket, room) => {
    const userList = io.rooms.get(room).users;
    if (!userList || userList.size >= MAXIMUM_USERS_FOR_ONE_ROOM || userList.size === 0) {
      socket.emit('JOIN_ROOM_ERROR', room);
      hideRoom(io, room);
      return;
    }
    userList.add(socket.username);
    const visible = (userList.size < MAXIMUM_USERS_FOR_ONE_ROOM);

    io.rooms.set(room, { visible, users: userList });
    socket.join(room);
    io[room].readyStatus[socket.username] = false;

    if (!visible) {
      io.sockets.emit("HIDE_ROOM", room);
    }
    else {
      io.sockets.emit("UPDATE_ROOM_USERS", room, userList.size);
    }  

    console.log(`${socket.username} joined ${room}`);
    socket.emit("ENTER_ROOM", room, io[room].readyStatus, socket.username);
    socket.to(room).emit('ADD_ROOM_USER', socket.username)
  };

  userLeftRoom = ({ io, username, room, commentator }) => {
    const userList = io.rooms.get(room).users;
    userList.delete(username);
    delete io[room].readyStatus[username];
    delete io[room].statistics[username];
    io[room].rating = io[room].rating.filter(us => us !== username);
    io.rooms.set(room, { visible: false, users: userList });
    if (io[room].isGameStarted || io[room].isTimerStarted) {
      commentator.missYou();
      commentator.unsubscribe(username);
    }
    if (io[room].rating.length === Object.keys(io[room].readyStatus).length) {
      finishGame(io, room, commentator);
    }
  
    if (userList.size === 0) {
      io.rooms.delete(room);
      delete io[room];
      roomFacade.deleteRoom(io, room);
    }
    else {
      if (!(io[room].isTimerStarted || io[room].isGameStarted)) {
        io.rooms.get(room).visible = true;
        io.emit("SHOW_ROOM", room);
      }
      io.sockets.emit("UPDATE_ROOM_USERS", room, userList.size);
      
      io.in(room).emit('USER_LEFT_ROOM', username);
      checkForTimerStart(io, room);
    }
  };

  uploadRooms = (io, socket) => {
    // Very long operations chain for transforming Set into and from JSON properly
    const transformedMap = JSON.stringify(Array.from(io.rooms).map( ([key, value]) => [key, { visible:value.visible, users: Array.from(value.users) } ] ) );
    socket.emit("UPLOAD_ROOMS", transformedMap);
  }

}

export const roomFacade = new RoomFacade();


import * as config from "./config";
import { roomFacade } from "./rooms";
import { toggleReadyStatus } from "./gamePreparation";
import { enterLetter } from "./gameProcess";
import { Commentator } from "./commentator";

const disconnectHandler = (io, username, commentator) => {
  const rooms = io.rooms;
  if (rooms.size === 0) {
    return;
  }
  const userRooms = Object.keys(Object.fromEntries(rooms.entries())).filter(room => rooms.get(room).users.has(username));
  if (userRooms.length === 0) {
    return;
  }
  const room = userRooms[0];
  roomFacade.userLeftRoom({io, username, room, commentator});
}

export default io => {
  io.rooms = new Map();
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const connectedUsernames = Object.values(io.sockets.sockets).map(el => el.username).filter(el => el);
    const commentator = new Commentator(io);
  
    if (connectedUsernames.indexOf(username) >= 0) {
      console.log('Error!');
      socket.emit("REAUTHORIZATION_ERROR", username);
    }
    else {
      socket.username = username;
    }

    roomFacade.uploadRooms(io, socket);
    
    socket.on('CREATING_ROOM', title => roomFacade.roomCreationHandler(io, socket, title));

    socket.on('USER_LEFT_ROOM', () => disconnectHandler(io, username, commentator));

    socket.on('JOIN_ROOM', room => roomFacade.joinRoomHandler(io, socket, room));

    socket.on('TOGGLE_READY_STATUS', () => toggleReadyStatus({ io, socket, commentator }));

    socket.on('disconnect', () => disconnectHandler(io, username, commentator));

    socket.on('ENTER_LETTER', obj => enterLetter(io, obj, commentator));

  });
}
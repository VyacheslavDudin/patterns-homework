import { texts } from './../data';

export const MAXIMUM_USERS_FOR_ONE_ROOM = 4;
export const SECONDS_TIMER_BEFORE_START_GAME = 10;
export const SECONDS_FOR_GAME = 60;
export const MAX_TEXT_ID = texts.length - 1;
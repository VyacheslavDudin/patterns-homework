import { SECONDS_FOR_GAME, MAXIMUM_USERS_FOR_ONE_ROOM } from "./config";
import { updateTimer } from "./gamePreparation";

export const startGame = async (io, room, commentator) => {
    io[room].isTimerStarted = false;
    io[room].isGameStarted = true;
    io.in(room).emit("GAME_START");
    const currPosCooldown = 40000;
    const jokeDelay = 3000;
    const getRate = () => Object.entries(io[room].statistics)
        .sort(([key1, val1],[key2, val2]) => val2.currSize-val1.currSize)
        .map(([key, val]) => key);
    commentator.startRace(commentator.roomMembers.map(el => el.username));

    commentator.interval = setInterval(() => {
        commentator.currPos(getRate());
         setTimeout(() => commentator.joke(), jokeDelay);
    }, currPosCooldown);
    await updateTimer(io, room, SECONDS_FOR_GAME, 'game-timer');
    const rate = getRate();
    io[room].rating.push(...rate);
    finishGame(io, room, commentator);

}

export const enterLetter = (io, { username, currSize, textSize }, commentator) => {
    let room = null;
    io.rooms.forEach((val, roomName) => {
        if (val.users.has(username)) {
            room = roomName;
        }
    });

    io[room].statistics[username] = { currSize, textSize };
    if ( textSize - currSize === 30) {
        commentator.closeToFinish(username);
    }
    if ( textSize - currSize === 5) {
        commentator.atFinishLine(username);
    }
    if (currSize === textSize) {
        io[room].rating.push(username);
        if (io[room].rating.length === Object.keys(io[room].readyStatus).length) {
            finishGame(io, room, commentator);
            return;
        }
    }

    io.in(room).emit("UPDATE_PROGRESS_BAR", username, currSize === textSize ? 100 : currSize / textSize * 100);
}

export const finishGame = (io, room, commentator) => {
    clearInterval(io[room].interval);
    clearInterval(commentator.interval);
    commentator.endOfGame(io[room].rating);
    commentator.roomMembers=[];
    io[room].isTimerStarted = false;
    io[room].isGameStarted = false;
    if ( !(io.rooms.get(room).users.size === MAXIMUM_USERS_FOR_ONE_ROOM)) {
        io.rooms.get(room).visible = true;
        io.emit("SHOW_ROOM", room);
    }
    
    Object.keys(io[room].readyStatus).forEach(user => {
        io[room].readyStatus[user] = false;
        io.to(room).emit("TOGGLE_INDICATORS", user, false);
        io.to(room).emit("UPDATE_PROGRESS_BAR", user, 0);
    });
    io[room].statistics = {};
    io[room].rating = [];
}
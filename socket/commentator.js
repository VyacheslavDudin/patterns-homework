// Using of Factory pattern
class EventSenderFactory {
    constructor (context) {
        this.context = context;
    }
    createSimpleEventSender = eventObj => this.context.broadcast.bind(this.context, eventObj);

    createParamEventSender = eventObj => param => {
        eventObj.textCreator(param);
        return this.context.broadcast.call(this.context, eventObj);
    };
}

// Made with using of the Observer pattern
export class Commentator {
    constructor (io) {
        this.eventSenderFactory = new EventSenderFactory(this);
        this.io = io;
        this.roomMembers = [];
        this.eventObjects = [
            { eventName:'GREETINGS', text: 'Усім привіт! З Вами на зв\'язку Ваш незмінний коментатор Василь Гречка! Поки наші \'Він Дизелі\' готуються до запеклого змагання, ми зручненько всідаємось та готуємося вболівати. Гонщики, які не стартануть - будуть дискваліфіковані!' },
            { 
                eventName:'START_RACE', 
                textCreator: function(users) {
                    let rest;
                    this.text = `Гонка розпочалась! Сьогодні, в ній беруть участь: ${users[0]} на велосипеді баби Палажки; 
                        ${users[1]? users[1] + ' на вінтажному самокаті' : ''} 
                        ${users[2]? ', а також' + users[2] + 'на легендарному вінику Гаррі Поттера - \'Nimbus 2001\' ': ''}
                        ${users.length > 3 ? [users[0], users[1], users[2], ...rest] = users : rest = '', rest}`;
                },
                text: ''
            },
            { 
                eventName:'CURRENT_POS',
                textCreator: function (liders) {
                    this.text = `Трійка лідерів на даний момент розміщена таким чином: ${liders?.join(', ')}`;
                },
                text: ''
            },
            { 
                eventName:'CLOSE_TO_FINISH_LINE',
                textCreator: function (username) {
                    this.text = `${username} вже наближається до фінішної прямої!`;
                },
                text: ''
            },
            {
                eventName:'AT_FINISH_LINE',
                textCreator: function (username) {
                    this.text =`${username} вже на фінішній прямій!`;
                }, 
                text: ''
            },
            {
                eventName:'END_OF_GAME',
                textCreator: function (rating) {
                    let places = '';
                    for (let i = 0; i < rating.length; i++) {
                        places += `#${i+1}: ${rating[i]}\n`;
                    }
                    this.text = `Кінець гри!\n ${places}`;
                },
                text: ''
            },
            { eventName:'JOKE', text: 'Мдаа.. Гонка вже скоро закінчиться, а наш гонщик все ніяк не доганяє. Не доганяє, що йому їхати в іншу сторону!' },
            { eventName:'MISS_YOU', text: 'Не всі витримують напруження.. Один з учасників вибуває з гонки! Я буду за ним сумувати :(' }
        ];
        // Using of Proxy pattern to have a possibility to get obj from array by one of properties(eventName)
        this.eventObjProxy = new Proxy(this.eventObjects, {
            get(arr, prop) {
                if (prop in arr) {
                    return arr[prop];
                } 
                else if (typeof prop === 'number') {
                    return undefined;
                }
                else {
                    let result = null;
                    for (let evtObj of arr) {
                        if (evtObj.eventName === prop) {
                            result = evtObj;
                        }
                    }
                    return result;
                }
            }
        });
        // Using of the currying 
        this.greetings = this.eventSenderFactory.createSimpleEventSender(this.eventObjProxy["GREETINGS"]);
        this.joke = this.eventSenderFactory.createSimpleEventSender(this.eventObjProxy["JOKE"]);
        this.missYou = this.eventSenderFactory.createSimpleEventSender(this.eventObjProxy["MISS_YOU"]);

        this.startRace = this.eventSenderFactory.createParamEventSender(this.eventObjProxy["START_RACE"]);
        this.currPos = this.eventSenderFactory.createParamEventSender(this.eventObjProxy["CURRENT_POS"]);
        this.closeToFinish = this.eventSenderFactory.createParamEventSender(this.eventObjProxy["CLOSE_TO_FINISH_LINE"]);
        this.atFinishLine = this.eventSenderFactory.createParamEventSender(this.eventObjProxy["AT_FINISH_LINE"]);
        this.endOfGame = this.eventSenderFactory.createParamEventSender(this.eventObjProxy["END_OF_GAME"]);
    }
    
    broadcast ({ eventName, text}) {
        this.roomMembers.forEach(user => {
            this.io.to(user.id).emit(eventName, text)
        });
    }

    unsubscribe (user) {
        this.roomMembers.forEach(({ id, username }, key) => {
            if (username === user) {
                this.roomMembers = this.roomMembers.filter(val => val !== this.roomMembers[key]);
            }
        })
      }
}

import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import { texts } from "./../data";

const router = Router();

router
  .get("/texts/:id", (req, res) => {
    res.status(200).send(JSON.stringify(texts[req.params.id]));
  })
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  });

export default router;

import { toggleIndicators } from "./gamePreperation.mjs";

let keypressExecutor = null;

export const startGame = async socket => {
    const mainScreen = document.querySelector('.main-screen');
    const timer = document.querySelector('.timer');
    const textDiv = document.querySelector('.text-block');
    const gameTimerDiv = document.createElement('div');
    const text = socket.text;

    timer.remove();
    gameTimerDiv.classList.add('game-timer-wrapper');
    gameTimerDiv.innerHTML = `<span class="game-timer"></span> seconds left`;
    mainScreen.appendChild(gameTimerDiv);
    textDiv.innerHTML = text;
    textDiv.classList.remove('display-none');

    const keypressHandler = (socket, text) => {
        const chars = [...text];
        const textSize = chars.length;
        const username = socket.query.username;
        let currSize = 0;
        let i = 0;
        let currLetter = chars[i];
        let nextLetter = chars[i+1];
        let enteredLetters = [];
        let isFinished = false;
        return event => {
            
            currLetter = chars[i];
            nextLetter = chars[i+1];
            if (isFinished || event.key !== currLetter) return;
    
            if (i === textSize - 1) {
                isFinished = true;
                nextLetter = [''];
            }
            else {
                i++;
            }
    
            enteredLetters.push(currLetter);
            currSize = enteredLetters.length;
            socket.emit("ENTER_LETTER", { username, currSize, textSize });

            const rest = isFinished ? [''] : [...(chars.join("").substr(currSize + 1, textSize - currSize))];
            const textBlock = document.querySelector('.text-block');
            textBlock.innerHTML = `<span class="entered-text">${enteredLetters.join("")}</span><u>${nextLetter}</u>${rest.join("")}`;
        };
    }
    keypressExecutor = keypressHandler(socket, text);
    document.addEventListener('keypress', keypressExecutor);
}

export const updateProgressBar = (username, percents) => {
    const user = Array.from(document.querySelectorAll('.user')).filter(user => user.username === username)[0];
    const progressBar = user.querySelector('.progress-bar-scale');
    progressBar.style.width = `${percents}%`;
    if (percents === 100) {
        progressBar.classList.add('finished');
    }
};

export const showResults = (socket, statistics) => {
    delete socket.text;
    document.removeEventListener('keypress', keypressExecutor);
    document.querySelector('.comments').innerHTML = statistics;
    const backBtn = document.querySelector('.btn-back');
    const readyBtn = document.querySelector('.btn-ready');
    backBtn.classList.remove('display-none');
    readyBtn.classList.remove('display-none');
    document.querySelector('.text-block').remove();
    document.querySelector('.game-timer-wrapper').remove();
    readyBtn.innerHTML = 'Ready';

    setTimeout(() => document.querySelector('.commentator').remove(), 4000);
};

export const finishGame = (socket, statistics, user) => {
    showResults(socket, statistics);
    updateProgressBar(user, 0);
    toggleIndicators(user, false);
}
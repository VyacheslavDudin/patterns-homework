export const toggleReadyStatus = socket => () => {
    document.querySelectorAll('.user').forEach(user => {
        if (user.username === socket.query.username) {
          const readyBtn = document.querySelector('.btn-ready');
          readyBtn.innerHTML === 'Ready' ? readyBtn.innerHTML = 'Not Ready' : readyBtn.innerHTML = 'Ready';          
        }
    });
    socket.emit("TOGGLE_READY_STATUS");
};

export const toggleIndicators = (username, isReady) => {
    document.querySelectorAll('.user').forEach(user => {
        if (user.username === username) {
            const indicator = user.querySelector('.indicator');
            if (isReady) {
                indicator.classList.remove('not-ready');
                indicator.classList.add('ready');
            }
            else {
                indicator.classList.add('not-ready');
                indicator.classList.remove('ready');
            }
        }
    });
};

export const startTimer = async (socket, id) => {
    const backBtn = document.querySelector('.btn-back');
    const readyBtn = document.querySelector('.btn-ready');
    const mainScreen = document.querySelector('.main-screen');
    const timer = document.createElement('div');
    
    const getTextUrl = `game/texts/${id}`;
    let text = 'null';
    let response = await fetch(getTextUrl);
    if (response.ok) {
        text = await response.json();
        socket.text = text;
    } else {
        alert("Ошибка HTTP: " + response.status);
    }

    backBtn.classList.add('display-none');
    readyBtn.classList.add('display-none');
    readyBtn.innerHTML = 'Ready';
    timer.classList.add('timer');
    mainScreen.appendChild(timer);

    const textDiv = document.createElement('div');
    textDiv.classList.add('text-block');
    textDiv.classList.add('display-none');
    mainScreen.appendChild(textDiv);
    
}

export const updateTimer = (seconds, timerClass) => {
    const timer = document.querySelector(`.${timerClass}`);
    timer.innerHTML = seconds;
}

export const createCommentsBlock = message => {
    const commentator = document.createElement('div');
    commentator.classList.add('commentator');
    commentator.innerHTML = `
        <img class="commentator-img" src="./../img/commentatorImg.jpg">
        <div class="comments">${message}</div>
    `;
    document.querySelector('.main').appendChild(commentator);
}
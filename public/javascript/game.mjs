import { reauthorizationErrorHandler, joinRoomErrorHandler, roomCreationErrorHandler } from "./errorHandlers.mjs";
import { uploadRooms, userLeftRoom, addRoomUser, backToRooms, createRoomHandler,
        enterRoom, updateRoomUsers, showRoom, hideRoom, deleteRoom, createRoom } from "./roomHelper.mjs";
import { toggleReadyStatus, toggleIndicators, startTimer, updateTimer, createCommentsBlock } from "./gamePreperation.mjs";
import { startGame, updateProgressBar, finishGame, showResults } from "./startGame.mjs";

const username = sessionStorage.getItem("username");
const socket = io("", { query: { username } });
const cardsBlock = document.querySelector('.room-cards');
const joinClickHandler = evt => {
  if (!evt.target.classList.contains('btn-join-room')) {
    return;
  }
  else {
    const title = Array.from(document.querySelectorAll('.room-card')).filter(card => card.querySelector('.btn-join-room') === evt.target)[0].title;
    document.querySelector('.btn-ready').innerHTML = 'Ready';
    socket.emit("JOIN_ROOM", title);
  }
};

const createRoomBtn = document.querySelector('.btn-create-room');
const createRoomExecuter = () => createRoomHandler(socket);
createRoomBtn.addEventListener('click', createRoomExecuter);


cardsBlock.addEventListener('click', joinClickHandler);
if (!username) {
  window.location.replace("/login");
}

document.querySelector('.btn-back').addEventListener('click', () => backToRooms(socket));
document.querySelector('.btn-ready').addEventListener('click', toggleReadyStatus(socket));

socket.on("REAUTHORIZATION_ERROR", reauthorizationErrorHandler);
socket.on("ROOM_CREATION_ERROR", roomCreationErrorHandler);
socket.on("JOIN_ROOM_ERROR", joinRoomErrorHandler);

socket.on("UPLOAD_ROOMS", uploadRooms);

socket.on("CREATE_ROOM", title => createRoom(socket, title));
socket.on("DELETE_ROOM", deleteRoom);
socket.on("UPDATE_ROOM_USERS", updateRoomUsers);
socket.on("HIDE_ROOM", hideRoom);
socket.on("SHOW_ROOM", showRoom);

socket.on("ENTER_ROOM", (title, users, username) => enterRoom(socket, title, users, username));
socket.on("BACK", () => backToRooms(socket));
socket.on("ADD_ROOM_USER", addRoomUser);
socket.on("USER_LEFT_ROOM", username => userLeftRoom(socket, username));

socket.on("TOGGLE_INDICATORS", toggleIndicators);

socket.on("TIMER_START", (id) => startTimer(socket, id));
socket.on("UPDATE_TIMER", updateTimer);
socket.on("GAME_START", () => startGame(socket));
socket.on("UPDATE_PROGRESS_BAR", updateProgressBar);
socket.on("SHOW_RESULTS", statistics => showResults(socket, statistics));
socket.on("FINISH_GAME", (statistics, user) => finishGame(socket, statistics, user));

socket.on("GREETINGS", message => createCommentsBlock(message));
socket.on("START_RACE", message => document.querySelector('.comments').innerHTML = message);
socket.on("MISS_YOU", message => document.querySelector('.comments').innerHTML = message);
socket.on("JOKE", message => document.querySelector('.comments').innerHTML = message);
socket.on("CURRENT_POS", message => document.querySelector('.comments').innerHTML = message);
socket.on("CLOSE_TO_FINISH_LINE", message => document.querySelector('.comments').innerHTML = message);
socket.on("AT_FINISH_LINE", message => document.querySelector('.comments').innerHTML = message);
socket.on("END_OF_GAME", statistics => showResults(socket, statistics));

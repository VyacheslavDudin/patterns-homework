export const findRoomCard = room => {
    const roomCards = Array.from(document.querySelectorAll('.room-card'));
    let res = null;
    roomCards.forEach(roomCard => roomCard.title === room ? res = roomCard : null) ;
    return res;
};
  
export const createRoom = (socket, title) => {
    const roomCardsDiv = document.querySelector('.room-cards');
    const roomCard = document.createElement("div");
    roomCard.classList.add("room-card");
    roomCard.title = title;
    roomCard.innerHTML =
        `    
        <p class="room-users"><span class="room-users-count">1 user</span> connected</p>
        <h3 class="room-title">${title}</h3>
        <button class="btn btn-join-room">Join</button>
        `;
    roomCardsDiv.appendChild(roomCard);
    socket.room = title;
};
  
export const deleteRoom = room => {
    const roomCard = findRoomCard(room);
    roomCard.remove();
};
  
export const hideRoom = room => {
    const roomCard = findRoomCard(room);
    roomCard.classList.add('display-none');
};
  
export const showRoom = room => {
    const roomCard = findRoomCard(room);
    roomCard.classList.remove('display-none');
};
  
export const updateRoomUsers = (room, usersCount) => {
    const roomCard = findRoomCard(room);
    roomCard.querySelector('.room-users-count').innerHTML = `${usersCount} ${usersCount === 1 ? 'user' : 'users'}`;
};
  
export const enterRoom = (socket, title, users, username) => {
    document.querySelector('#rooms-page').classList.add('display-none');
    document.querySelector('#game-page').classList.remove('display-none');
    const usersBlock = document.querySelector('.users');
    document.querySelector('.game-title').innerHTML = title;
    Object.entries(users).forEach(([user, readyStatus]) => {
      const indColor = readyStatus ? 'ready' : 'not-ready';
      const userDiv = document.createElement('div');
      userDiv.classList.add('user');
      userDiv.username = user;
      userDiv.innerHTML = `
          <div class="user-info">
            <span class="indicator ${indColor}"></span>
            <span class="name">${user === username ? `${username} (you)` : user}</span>
          </div>
          <div class="progress-bar">
            <div class="progress-bar-scale"></div>
          </div>
        `;
        usersBlock.appendChild(userDiv);
    });
    socket.room = title;
};
  
export const addRoomUser = username => {
    const usersBlock = document.querySelector('.users');
    const userDiv = document.createElement('div');
    userDiv.classList.add('user');
    userDiv.username = username;
    userDiv.innerHTML = `
        <div class="user-info">
          <span class="indicator not-ready"></span>
          <span class="name">${username}</span>
        </div>
        <div class="progress-bar">
          <div class="progress-bar-scale"></div>
        </div>
      `;
      usersBlock.appendChild(userDiv);
};
  
export const userLeftRoom = (socket, username) => {
    document.querySelectorAll('.user').forEach(user => {
      if (user.username === username) {
        user.remove();
      }
    });
};

export const backToRooms = socket => {
  document.querySelector('#rooms-page').classList.remove('display-none');
  document.querySelector('#game-page').classList.add('display-none');
  const usersBlock = document.querySelector('.users');
  usersBlock.innerHTML = '';
  socket.emit( "USER_LEFT_ROOM");
};

export const createRoomHandler = socket => {
  const title = prompt('Enter the name of the room:', '').trim();
  if (title.length === 0) {
    alert('Error! The name of the room have to be not empty! Try again!');
    return;
  }
  else {
    socket.emit("CREATING_ROOM", title);
  }
};

export const uploadRooms = _rooms => {
    // Very long operations chain for transforming Set into and from JSON properly
    const rooms = new Map(JSON.parse(_rooms).map( ([key, val]) => [key, {...val, users: new Set(val.users)}]) );
    const roomCardsDiv = document.querySelector('.room-cards');
    for (let [room, value] of rooms.entries()) {
      const visible = (value.visible === true);
      const roomCard = document.createElement("div");
      const usersCount = value.users.size;
      roomCard.classList.add(`room-card`);
      !visible ? roomCard.classList.add(`display-none`) : '';
      roomCard.title = room;
      roomCard.innerHTML =
        `    
          <p class="room-users"><span class="room-users-count">${usersCount} ${usersCount === 1 ? 'user' : 'users'}</span> connected</p>
          <h3 class="room-title">${room}</h3>
          <button class="btn btn-join-room">Join</button>
        `;
      roomCardsDiv.appendChild(roomCard);
    }
};
  